require 'compass/import-once/activate'
require 'autoprefixer-rails' # gem install autoprefixer-rails

http_path = "./"
http_images_dir = "../img/"

css_dir = "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"

encoding = 'windows-1251'

# auto encoding external in UTF-8
Encoding.default_external = 'utf-8'

relative_assets  = false
output_style = :nested #:compact #:compressed
line_comments = false

# Запуск автопрефиксера https://github.com/postcss/autoprefixer#compass
on_stylesheet_saved do |file|
	css = File.read(file)
	map = file + '.map'

	if File.exists? map
		result = AutoprefixerRails.process(css,
			from: file,
			to:   file,
			remove: false,
			map:  { prev: File.read(map), inline: false})
		File.open(file, 'w') { |io| io << result.css }
		File.open(map,  'w') { |io| io << result.map }
	else
		File.open(file, 'w') { |io| io << AutoprefixerRails.process(css, remove: false) }
	end
end
