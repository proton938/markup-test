$(function(){
    $('.js-form').each(function(){
        var $form = $(this).ajaxForm({
                beforeSubmit : function(fields){
                    $.each(fields, function(i, field){
                        if(field.name == 'confirm'){
                            field.value = 0;
                        }
                    });
                    $form.addClass('loading');
                },
                success : function(response){
                    $form.removeClass('loading');
                    if(response.ok == '1'){
                        $form.addClass('form-complete');
                        var onsuccess = $form.attr('data-onsuccess');
                        if(onsuccess && onsuccess.length){
                            eval(onsuccess);
                        }
                        $form.find('input[type=text], textarea').val('');
                    }
                    else{
                        $form.addClass('error');
                        if(response.fields && response.fields.length){
                            $.each(response.fields, function(i, field){
                                $form.find('[name=' + field.name + ']').addClass('error');
                            });
                        }
                    }
                    captcha.update();
                }
            }),
            captcha = {
                ok : false,
                _init : function(){
                    var self = this;
                    this.e = {
                        image : $form.find('.js-captcha-image'),
                        text : $form.find('.js-captcha-text'),
                        code : $form.find('.js-captcha-code')
                    };
                    if(!this.e.text.length || !this.e.image.length || !this.e.code.length){
                        return;
                    }
                    this.ok = true;
                    this.e.image.click(function(){
                        self.update();
                    })
                },
                update : function(){
                    if(!this.ok){
                        return;
                    }
                    var self = this;
                    $.post('/ajax.php', {
                        'get_captcha' : '1'
                    }, function(id){
                        self.e.text.val('');
                        self.e.image.attr('src', '/bitrix/tools/captcha.php?captcha_sid=' + id);
                        self.e.code.val(id);
                    });
                }
            },
            onchange = function(){
                $(this).removeClass('error');
            };

        captcha._init();

        $form.find('input, textarea').change(onchange).keyup(onchange);
    });
})