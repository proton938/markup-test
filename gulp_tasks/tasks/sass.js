var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('./browserSync');
var config = require('../config');

var isLocal = config.env.isLocal;
var browsers = config.browsers;
var paths = isLocal ? config.localPaths : config.paths;

gulp.task('sass', function () {
  gulp.src(['*.scss', '!_*.scss', 'helpers/*.scss', '!helpers/_*.scss'], {cwd: paths.sass})
    .pipe(sass({
    	outputStyle: 'expanded'
    }).on('error', sass.logError))
		.pipe(autoprefixer({
      browsers: browsers,
      cascade: true,
      remove: false
    }))
    .pipe(gulp.dest(paths.css))
    .pipe(browserSync.stream());
});
